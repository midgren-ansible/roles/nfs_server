# NFS Server

Ansible role to setup an NFS server in a FreeIPA domain.

This role depends on nfs_client, so there is no need to apply both roles.
